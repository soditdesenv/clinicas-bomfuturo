const gulp = require('gulp')
const uglify = require('gulp-uglify')
const uglifycss = require('gulp-uglifycss')
const concat = require('gulp-concat')
const babel = require('gulp-babel')
const sass = require('gulp-sass')

gulp.task('app', ['app.assets', 'app.js', 'app.css'])

gulp.task('app.sass', function () {
    return gulp.src(['sass/main.scss'])
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('app/css'))
})

gulp.task('app.css',['app.sass'], function(){
    return gulp.src(['app/**/*.css'])
        .pipe(uglifycss({ "uglyComments": true}))
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('public/assets/css'))
})

gulp.task('app.js', function(){
    return gulp.src(['app/**/*.js'])
        .pipe(babel({ presets: ['es2015'] }))
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('public/assets/js'));
});

gulp.task('app.assets', function(){
    return gulp.src(['assets/**/*.*'])
        .pipe(gulp.dest('public/assets'));
}); 