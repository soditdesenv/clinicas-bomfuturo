const gulp = require('gulp')

require('./gulpTasks/app')
require('./gulpTasks/server')

gulp.task('default', function(){
    gulp.start('app', 'server');
})